from robot import Robot

class Cache:

    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        if url not in self.cache:
            robot = Robot(url = url)
            self.cache[url] = robot

    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()

    def show(self, url):
        print(self.content(url))

    def show_all(self):
        for url in self.cache:
            print(url)