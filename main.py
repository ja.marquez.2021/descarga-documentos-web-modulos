from cache import Cache
from robot import Robot

if __name__ == '__main__':
    print("Test Robot class")
    r = Robot('http://gsyc.urjc.es/')
    print(r.url)
    r.show()
    r.retrieve()
    r.retrieve()
    print("Test Cache class")
    c = Cache()
    c.retrieve('http://gsyc.urjc.es/')
    c.show('https://www.aulavirtual.urjc.es')
    c.show_all()