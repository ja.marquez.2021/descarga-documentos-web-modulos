import urllib.request

class Robot:

    def __init__(self, url):
        self.url = url
        self.retrieved = False
        print(self.url)

    def retrieve(self):
        if not self.retrieved:
            print("Descargando...")
            f = urllib.request.urlopen(self.url)
            #aqui he creado una instacia de una clase? si es afirmativo, entonces puedo crear instancias sobre la marcha?
            self.content = f.read().decode('utf-8')
            self.retrieved = True

    def content(self):
        self.retrieve()
        return self.content

    def show(self):
        print(self.content())